<?php

/**
 * Implements  hook_permission().
 */
function distributed_blocks_permission() {
  return array(
    'administer distributed blocks' => array(
      'title' => t('Administer Distributed Blocks'),
      'description' => t('Change master/client settings for this module.')
    ),
  );
}

/**
 * Implements  hook_menu()
 */
function distributed_blocks_menu() {
  $items['admin/config/system/distributed_blocks'] = array(
    'title' => 'Distributed Blocks',
    'description' => 'Configure settings for distributing blocks across sites.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('distributed_blocks_admin_form'),
    'file' => 'distributed_blocks.admin.inc',
    'access arguments' => array('administer distributed blocks'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['distributed_blocks/render/%'] = array(
    'title' => 'Render block HTML',
    'page callback' => 'distributed_blocks_render_block',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'delivery callback' => 'drupal_json_output',
  );
  $items['distributed_blocks/list'] = array(
    'title' => 'Return a list of available blocks',
    'page callback' => 'distributed_blocks_list_blocks',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'delivery callback' => 'drupal_json_output',
  );
  return $items;
}

/**
 * Callback used to generate block HTML.
 */
function distributed_blocks_render_block($block_id, $json = FALSE) {
  // Do not expose any blocks
  if (!variable_get('distributed_blocks_expose', FALSE)) {
    drupal_add_http_header('Status', '403 Forbidden');
    return "DISABLED";
  }

  // Check against blocks that are exposed
  $exposed_blocks = variable_get('distributed_blocks_exposed_blocks', array());

  if (empty($exposed_blocks[$block_id])) {
    drupal_add_http_header('Status', '404 Not Found');
    return 'NOT_FOUND';
  }

  // The id is provided in MODULE--DELTA--BLOCK TITLE format
  $block_id = explode('--', $block_id);
  $block = block_load($block_id[0], $block_id[1]);
  if (!$block) {
    drupal_add_http_header('Status', '404 Not Found');
    return 'NOT_FOUND';
  }

  $existing_css = drupal_add_css();
  $array = module_invoke($block->module, 'block_view', $block->delta);
  $delta = str_replace('-', '_', $block->delta);
  drupal_alter(array('block_view', "block_view_{$block->module}_{$delta}"), $array, $block);

  $block_content = is_array($array['content']) ? drupal_render($array['content']) : $array['content'];
  $all_css = drupal_add_css();
  $new_css = array_diff_key($all_css, $existing_css);

  // Force absolute URLs in exposed blocks
  if (variable_get('distributed_blocks_force_absolute_links', FALSE)) {
    $block_content = str_replace(' href="/', ' href="http://' . $_SERVER['HTTP_HOST'] . '/', $block_content);
  }

  $return = array(
    'subject' => $array['subject'],
    'content' => $block_content,
    // New css is css that was added during block_view stage of block. 
    'new css' => array(
      'definition' => $new_css,
      'rendered' => drupal_get_css($new_css),
    ),
    'all css' => array(
      'definition' => $all_css,
      'rendered' => drupal_get_css($all_css),
    ),
  );
  drupal_add_http_header('Drupal-Response-Key', distributed_blocks_generate_hash($return));
  return $return;

}

/**
 * Callback that returns a list of available blocks from this site in JSON format.
 */
function distributed_blocks_list_blocks() {
  $exposed_blocks = array_filter(variable_get('distributed_blocks_exposed_blocks', array()));
  if (empty($exposed_blocks)) {
    return 'NO_EXPOSED_BLOCKS';
  }
  else {
    $all_blocks = _distributed_blocks_get_all_blocks();
    $return = array_intersect_key($all_blocks, array_filter($exposed_blocks));
  }
  drupal_add_http_header('Drupal-Response-Key', distributed_blocks_generate_hash($return));
  return $return;
}

/**
 * Refresh the cache for all blocks.
 */
function distributed_blocks_fetch_blocks() {
  $fetched_blocks = variable_get('distributed_blocks_fetched_blocks', array());
  foreach ($fetched_blocks as $block) {
    distributed_blocks_fetch_block($block, TRUE);
  }
}

/**
 * Fetches distributed block from master site.
 */
function distributed_blocks_fetch_block($block, $reset = FALSE) {
  $cache_key = 'distributed_blocks_block_' . $block;
  // Not bothering with static cache since should only be called once.
  if (!$reset && ($cache = cache_get($cache_key))) {
    return $cache->data;
  }

  $master_url = variable_get('distributed_blocks_master_url', '');
  if (!$master_url) {
    $result['message'] = t('Request failed, master site is not configured.');
    return $result;
  }

  // The drupal_encode_path had to be added, so much for url() doing it
  $url = url($master_url . '/distributed_blocks/render/' . drupal_encode_path($block));
  $response = drupal_http_request($url);

  $return = FALSE;
  if ($response->code != 200) {
    watchdog('distributed_blocks', 'Request failed, returned HTTP status code @code. Please check that your master site has Distributed Blocks enabled.', array('@code' => $response->code));
  }
  if ($response->data == 'NOT_FOUND') {
    watchdog('distributed_blocks', 'Block not exposed or found on master site. Argument used: @block', array('@block' => $block));
  }
  if ($response->data == 'DISABLED') {
    watchdog('distributed_blocks', 'All blocks disabled on this site. Argument used: @block', array('@block' => $block));
  }
  elseif (!distributed_blocks_test_response_key($response)) {
    watchdog('distributed_blocks', 'Response key mistmatch, please make sure variable distributed_blocks_secret_key is set to the same variable across sites ', array('@block' => $block));
  }
  else {
    if ($decoded = drupal_json_decode($response->data)) {
      $return = $decoded;
    }
    // Older versions of distrubuted blocks returned just the content.
    elseif (is_string($response->data)) {
      $return = array('content' => $response->data);
    }
  }
  cache_set($cache_key, $return);
  return $return;
}

/**
 * Verify that the response has the correct key.
 *
 * @param $response
 *  The response object.
 */
function distributed_blocks_test_response_key($response) {
  // If not configured to check response key, exit out.
  if (!variable_get('distributed_blocks_check_key', FALSE)) {
    return TRUE;
  }
  elseif (empty($response->headers['drupal-response-key'])) {
    return FALSE;
  }
  return distributed_blocks_verify_hash($response->headers['drupal-response-key'], $response->data);
}

/**
 * Generate the hash for the data.
 *
 * @param string $data
 *
 * @return string
 */
function distributed_blocks_generate_hash($data) {
  return hash('sha256', variable_get('distributed_blocks_secret_key', '') . (!is_string($data) ? drupal_json_encode($data) : $data));
}

/**
 * Verify that the hash provided by the menu provider matches our computation.
 *
 * @param $hash
 * @param $content
 *
 * @return bool
 */
function distributed_blocks_verify_hash($hash, $content) {
  return $hash === distributed_blocks_generate_hash($content);
}

/**
 * Fetch the block list from the master site.
 *
 * @param $master_url
 *  The url to the site; set if not currently the variable.
 * @param $set_form_error
 *  Internal only, set the form error.
 * @param $reset
 *  Reset the caches for this info.
 */
function distributed_blocks_fetch_master_block_list($master_url = NULL, $set_form_error = FALSE, $reset = FALSE) {
  $blocks = &drupal_static(__FUNCTION__, NULL);
  if (!$reset && !isset($blocks) && ($cache = cache_get(__FUNCTION__))) {
    $blocks = $cache->data;
    return $blocks;
  }
  elseif ($reset || !isset($blocks)) {
    $blocks = array();
    if (!$master_url) {
      $master_url = variable_get('distributed_blocks_master_url', '');
      if (!$master_url) {
        cache_set(__FUNCTION__, $blocks);
        return $blocks;
      }
    }
    $url = $master_url . '/distributed_blocks/list';
    $response = drupal_http_request($url);
    $message = FALSE;
    if ($response->code != '200') {
      $message = t('Request failed, returned HTTP status code @code. Please check that your master site has Distributed Blocks enabled.', array('@code' => $response->code));
    }
    elseif (!distributed_blocks_test_response_key($response)) {
       $message = t('Response key mismatch, please make sure variable distributed_blocks_secret_key is set to the same value across sites ');
    }
    else {
      $resp_array = drupal_json_decode($response->data);
      if (!$resp_array) {
        $message = t('Failed to decode JSON data retrieved from master site.');
      }
      elseif ($resp_array == 'NO_EXPOSED_BLOCKS') {
        $message = t('No exposed blocks on the master site.');
      }
    }
    if ($message) {
      if ($set_form_error) {
        form_set_error('distributed_blocks_master_url', $message);
      }
      watchdog('distributed_blocks', $message, NULL, WATCHDOG_WARNING);
    }
    else {
      $blocks = $resp_array;
    }
    cache_set(__FUNCTION__, $blocks);
  }
  return $blocks;
}

/**
 * Returns the delta for a block.
 *
 * There is a 32 limit on delta length so since adding many characters,
 * need a delta that can be transfered back.
 */
function distributed_blocks_get_delta($block) {
  $delta = 'd_b--' . $block;
  // delta can only be 32 chars
  if (strlen($delta) > 32) {
    $delta = md5($delta);
    // Same method views uses.
    $old_hashes = $hashes = variable_get('distributed_blocks_hashes', array());
    $hashes[$block] = $delta;
    if ($hashes != $old_hashes) {
      variable_set('distributed_blocks_hashes', $hashes);
    }
  }
  return $delta;
}

/**
 * Returns the block for a delta.
 *
 * There is a 32 limit on delta length so since adding many characters,
 * need a delta that can be transfered back.
 */
function distributed_blocks_get_block_from_delta($delta) {
  if (strpos($delta, 'd_b--') === 0) {
    return substr($delta, 5);
  }
  elseif (($hashes = variable_get('distributed_blocks_hashes', array())) && ($block = array_search($delta, $hashes))) {
    return $block;
  }
  return FALSE;
}

/**
 * Implements hook_block_info().
 *
 * Programmatically instantiates blocks from cached fetched stuff.
 */
function distributed_blocks_block_info() {
  $blocks = array();
  // Only provide blocks for enabled and available blocks.
  $valid_blocks = distributed_blocks_fetch_master_block_list();
  $fetched_blocks = array_intersect(array_filter(variable_get('distributed_blocks_fetched_blocks', array())), array_keys($valid_blocks));

  foreach ($fetched_blocks as $block) {
    $blocks[distributed_blocks_get_delta($block)] = array(
      'info' => 'Distributed Block - ' . $valid_blocks[$block],
      'cache' => DRUPAL_CACHE_GLOBAL,
    );
  }

  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * Programmatically instantiates blocks from cached fetched stuff.
 */
function distributed_blocks_block_view($delta = '') {
  if ($block = distributed_blocks_get_block_from_delta($delta)) {
    $valid_blocks = distributed_blocks_fetch_master_block_list();
    $fetched_blocks = array_intersect(array_filter(variable_get('distributed_blocks_fetched_blocks', array())), array_keys($valid_blocks));
    return !empty($fetched_blocks[$block]) ? distributed_blocks_fetch_block($block) : NULL;
  }
}

/**
 * Utility function that returns all blocks available on this site as an options array.
 */
function _distributed_blocks_get_all_blocks() {
  global $theme_key;
  static $options = array();

  if (empty($options)) {
    $blocks = _block_rehash($theme_key);
    foreach ($blocks as $block) {
      $options[$block['module'] . '--' . $block['delta']] = $block['info'] . ' (' . $block['module'] . ' ' . $block['delta'] . ')';
    }
  }
  return $options;
}
