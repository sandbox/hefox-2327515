<?php
/**
 * @file
 *
 * Contains admin page code for the distributed blocks module.
 */

/**
 * Admin form generation callback
 */
function distributed_blocks_admin_form($form, &$form_state) {
  $form['distributed_blocks_expose'] = array(
    '#type' => 'checkbox',
    '#title' => t('This site exposes its blocks for sharing'),
    '#description' => t('If checked, this site will be designated the master that client sites will sync against.'),
    '#default_value' => variable_get('distributed_blocks_expose', FALSE),
  );
  $show_when_exposed = array(
    'visible' => array(
      ':input[name="distributed_blocks_expose"]' => array('checked' => TRUE),
    ),
  );
  $form['distributed_blocks_check_key'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verify Key'),
    '#description' => t('For added security, use a key to verify the response from the master site. <strong>You must</strong> add $conf["distributed_blocks_secret_key"] = "a random long string"; to your settings.php file for both master and child sites.'),
    '#default_value' => variable_get('distributed_blocks_check_key', FALSE),
    '#states' => $show_when_exposed,
  );
  $form['distributed_blocks_force_absolute_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force absolute links in exposed blocks'),
    '#description' => t('If checked, links in exposed blocks will be parsed and reformatted into absolute URLs.'),
    '#default_value' => variable_get('distributed_blocks_force_absolute_links', FALSE),
    '#states' => $show_when_exposed,
  );
  $form['distributed_blocks_master'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select exposed blocks from this site'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => $show_when_exposed,
  );

  // Get all blocks available on this site for the current theme
  $options_blocks = _distributed_blocks_get_all_blocks();

  $form['distributed_blocks_master']['distributed_blocks_exposed_blocks'] = array(
    '#type' => 'checkboxes',
    '#title' => t("Exposed Blocks"),
    '#description' => t('Select blocks from this site that should be exposed individually.'),
    '#options' => $options_blocks,
    '#default_value' => variable_get('distributed_blocks_exposed_blocks', array()),
  );

  $form['distributed_blocks_client'] = array(
    '#type' => 'fieldset',
    '#title' => t('Client settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  //TODO: Do a fancypants AJAX version of this
  $form['distributed_blocks_client']['distributed_blocks_master_url'] = array(
    '#type' => 'textfield',
    '#title' => t("Master site URL"),
    '#description' => t('This is the full URL (ie http://www.example.com) of the site you want to fetch exposed
    blocks from. Only set this if this is a client site.'),
    '#default_value' => variable_get('distributed_blocks_master_url', ''),
  );

  // Only display available blocks if the master site is set and valid
  if (variable_get('distributed_blocks_master_url', '')) {
    if ($options = distributed_blocks_fetch_master_block_list()) {
      $form['distributed_blocks_client']['distributed_blocks_fetched_blocks'] = array(
        '#type' => 'checkboxes',
        '#title' => t("Fetched Blocks"),
        '#description' => t('Select blocks that you want to fetch from the master site.'),
        '#options' => $options,
        '#default_value' => variable_get('distributed_blocks_fetched_blocks', array()),
      );
    }
    else {
      $form['distributed_blocks_client']['distributed_blocks_fetched_blocks'] = array(
        '#type' => 'item',
        '#title' => t("Fetched Blocks"),
        '#value' => t('No blocks found on master site.'),
      );
    }
    $form['distributed_blocks_client']['refresh'] = array(
      '#type' => 'submit',
      '#value' => t('Refresh the fetched blocks and block list.'),
      '#submit' => array('distributed_blocks_refresh_submit'),
    );
  }

  return system_settings_form($form);
}

/**
 * Admin form validation callback
 */
function distributed_blocks_admin_form_validate($form, &$form_state) {
  // Only allow checking key if secret key is set.
  if (!empty($form_state['values']['distributed_blocks_check_key']) && !variable_get('distributed_blocks_secret_key')) {
    form_set_error('distributed_blocks_check_key', t('distributed_blocks_secret_key is not configured. Please add it to settings.php.'));
  }

  // Clean up the value to make a cleaner variable export.
  if (!empty($form_state['values']['distributed_blocks_exposed_blocks'])) {
    $form_state['values']['distributed_blocks_exposed_blocks'] = array_filter($form_state['values']['distributed_blocks_exposed_blocks']);
  }
  if (!empty($form_state['values']['distributed_blocks_fetched_blocks'])) {
    $form_state['values']['distributed_blocks_fetched_blocks'] = array_filter($form_state['values']['distributed_blocks_fetched_blocks']);
  }

  // Validate source URL
  $master_url = $form_state['values']['distributed_blocks_master_url'];

  // If this value is not set, you are considered the master site
  if (empty($master_url)) {
    return;
  }

  if (!empty($master_url) && !valid_url($master_url, TRUE)) {
    form_set_error('distributed_blocks_master_url', t('Invalid URL given.'));
    return;
  }

  // Trigger the form error if exists.
  distributed_blocks_fetch_master_block_list($master_url, TRUE, TRUE);
}

/**
 * Submit callback to trigger block fetching from settings screen.
 */
function distributed_blocks_refresh_submit($form, &$form_state) {
  $res = distributed_blocks_fetch_blocks();
  distributed_blocks_fetch_master_block_list(NULL, TRUE, TRUE);
  drupal_goto('admin/config/system/distributed_blocks');
}
